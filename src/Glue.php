<?php
namespace Nerd;

/**
 * Glue
 * ====
 * 
 * The Glue class is responsible for integration between everything in your app.
 * It provides centralized event listening and emitting, and avoids unnessecary
 * class loading if possible.
 * 
 * With the Glue class, you can attach event listeners on classes that have not
 * yet been loaded. Sometimes those classes will never be loaded, and by using
 * the Glue class - PHP is not required to load those classes.
 * 
 * Also, Glue allows you to provide factory methods for a long range of
 * libraries, allowing you to properly utilize dependency injection.
 * 
 * Events
 * ------
 * 
 * Glue::on(Some\Class::class, 'template.rendered', function($e) {
 *    // Handle your event
 * });
 * 
 * Glue::emit(Some\Class:class, 'template.rendered', ["event" => "data"]);
 * 
 * Dependency Injection
 * --------------------
 * This is by no means a full Dependency Injection implementation, however it
 * provides one of the main benefits of Dependency Injection - allowing you to
 * replace for example the Caching implementation with another implementation
 * via configuration.
 * 
 * If your system consistently calls Glue::create(CacheInterface::class) to
 * retrieve the caching backend - it will be easy to replace the caching backend
 * throughout your application via configuration.
 * 
 * // Create a factory
 * Glue::factory(CacheInterface::class, function($namespace) {
 *   // Do the work to create an instance of the class
 *   return new Cache($namespace);
 * });
 * 
 * // Get the Cache instance
 * $cache = Glue::get(CacheInterface::class);
 * 
 */
class Glue {
    
    /**
     * Factory functionality
     */
     
    /**
     * Contains a list of factories for certain classes
     */
    protected static $factories = [];
     
    /**
     * Create a factory for an interface. This factory will create new instances
     * for every call. If you need singletons, you can use the singletonFactory()
     * method.
     * 
     * @param class $interfaceName
     * @param callable $factory
     */
    public static function instanceFactory($interfaceName, $factory) {
        return self::$factories[$interfaceName] = $factory;
    }
    
    /**
     * Create a singleton factory. Singletons can't use arguments when you 
     * retrieve them, since that would defeat the Singleton concept.
     * 
     * @param class $interfaceName
     * @param callable $factory
     */
    public static function singletonFactory($interfaceName, $factory) {
        $callable = function() use ($factory) {
            static $instance;
            if($instance) return $instance;
            
            return $instance = call_user_func($factory);
        };
        
        return self::instanceFactory($interfaceName, $callable);
    }
    
    /**
     * Get an instance of a class
     * 
     * @param class $interfaceName
     * @param mixed ... Arguments required to create the instance
     * @return object
     */
    public static function get($interfaceName) {
        if(isset(self::$factories[$interfaceName])) {

            $callable = self::$factories[$interfaceName];

	} else if(isset($GLOBALS['Glue:'.$interfaceName])) {

            $callable = $GLOBALS['Glue:'.$interfaceName];

        } else {
            throw new Exception("No such factory installed.");
        }

        $callable = self::$factories[$interfaceName];
        $arguments = func_get_args();

        // The first argument is the interface name, so we remove it
        array_shift($arguments);

        return call_user_func_array($callable, $arguments);
    }
    
    /**
     * Event functionality
     */
     
     /**
      * Creates the EventEmitter singleton
      */
/*
    protected static function getEventEmitter() {
        static $instance;
        if($instance) return $instance;
        
        return $instance = new Fubber\Events\EventEmitter();
    }
*/
    /**
     * Add an event listener
     * 
     * @param class $eventClass
     * @param string $eventName
     * @param callable $eventHandler
     * @return null
     */
/*    public static function on($eventClass, $eventName, $eventHandler) {
        return self::getEventEmitter()->on($eventClass.'.'.$eventName, $eventHandler);
    }
*/
    /**
     * Create a special event listener that removes itself after being invoked
     * 
     * @param class $eventClass
     * @param string $eventName
     * @param callable $eventHandler
     * @return null
     */
/*    public static function once($eventClass, $eventName, $eventHandler) {
        return self::getEventEmitter()->once($eventClass.'.'.$eventName, $eventHandler);
    }
*/
    /**
     * Remove a spesific event listener
     * 
     * @param class $eventClass
     * @param string $eventName
     * @param callable $eventHandler
     * @return null
     */
/*    public static function removeListener($eventClass, $eventName, $eventHandler) {
        return self::getEventEmitter()->removeListener($eventClass.'.'.$eventName, $eventHandler);
    }
*/
    /**
     * Remove all event listeners for a specific event
     * 
     * @param class $eventClass
     * @param string $eventName
     * @return null
     */
/*    public static function removeAllListeners($eventClass, $eventName) {
        return self::getEventEmitter()->removeAllListeners($eventClass.'.'.$eventName);
    }
*/
}
