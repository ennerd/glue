<?php
require("../vendor/autoload.php");
use \Nerd\Glue;

class GlueTest extends PHPUnit_Framework_TestCase {
	public function testCreateInstanceFactory() {
		Glue::instanceFactory(\DateTime::class, function() {
			return new DateTime();
		});

		$this->assertTrue(get_class($first = Glue::get(\DateTime::class))=="DateTime");
		sleep(1);
		$this->assertTrue($first < Glue::get(\DateTime::class));
	}

	/**
	*	This tests relies on the fact that the first factory is overwritten
	*/
	public function testCreateSingletonFactory() {
		Glue::singletonFactory(\DateTime::class, function() {
			return new DateTime();
		});

		$this->assertTrue(get_class($first = Glue::get(\DateTime::class))=="DateTime");
		sleep(1);
		$this->assertTrue($first == Glue::get(\DateTime::class));

	}
}
